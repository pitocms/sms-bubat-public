<div class="departments view large-9 medium-8 columns content">
    <h3><?= h($department->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($department->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($department->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($department->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($department->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Routines') ?></h4>
        <?php if (!empty($department->routines)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Department Id') ?></th>
                <th scope="col"><?= __('Semester Id') ?></th>
                <th scope="col"><?= __('Subject Id') ?></th>
                <th scope="col"><?= __('Teacher Id') ?></th>
                <th scope="col"><?= __('Room Id') ?></th>
                <th scope="col"><?= __('Day Id') ?></th>
                <th scope="col"><?= __('Starting Hour') ?></th>
                <th scope="col"><?= __('Ending Hour') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($department->routines as $routines): ?>
            <tr>
                <td><?= h($routines->id) ?></td>
                <td><?= h($routines->department_id) ?></td>
                <td><?= h($routines->semester_id) ?></td>
                <td><?= h($routines->subject_id) ?></td>
                <td><?= h($routines->teacher_id) ?></td>
                <td><?= h($routines->room_id) ?></td>
                <td><?= h($routines->day_id) ?></td>
                <td><?= h($routines->starting_hour) ?></td>
                <td><?= h($routines->ending_hour) ?></td>
                <td><?= h($routines->created) ?></td>
                <td><?= h($routines->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Routines', 'action' => 'view', $routines->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Routines', 'action' => 'edit', $routines->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Routines', 'action' => 'delete', $routines->id], ['confirm' => __('Are you sure you want to delete # {0}?', $routines->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
    <div class="related">
        <h4><?= __('Related User Profiles') ?></h4>
        <?php if (!empty($department->user_profiles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Designation') ?></th>
                <th scope="col"><?= __('Department Id') ?></th>
                <th scope="col"><?= __('Division Id') ?></th>
                <th scope="col"><?= __('Phone Number') ?></th>
                <th scope="col"><?= __('Gender') ?></th>
                <th scope="col"><?= __('Blood Group') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('User Type') ?></th>
                <th scope="col"><?= __('Semester') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($department->user_profiles as $userProfiles): ?>
            <tr>
                <td><?= h($userProfiles->id) ?></td>
                <td><?= h($userProfiles->user_id) ?></td>
                <td><?= h($userProfiles->name) ?></td>
                <td><?= h($userProfiles->designation) ?></td>
                <td><?= h($userProfiles->department_id) ?></td>
                <td><?= h($userProfiles->division_id) ?></td>
                <td><?= h($userProfiles->phone_number) ?></td>
                <td><?= h($userProfiles->gender) ?></td>
                <td><?= h($userProfiles->blood_group) ?></td>
                <td><?= h($userProfiles->address) ?></td>
                <td><?= h($userProfiles->user_type) ?></td>
                <td><?= h($userProfiles->semester) ?></td>
                <td><?= h($userProfiles->created) ?></td>
                <td><?= h($userProfiles->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserProfiles', 'action' => 'view', $userProfiles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserProfiles', 'action' => 'edit', $userProfiles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserProfiles', 'action' => 'delete', $userProfiles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userProfiles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
