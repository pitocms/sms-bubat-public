<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Division $division
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Division'), ['action' => 'edit', $division->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Division'), ['action' => 'delete', $division->id], ['confirm' => __('Are you sure you want to delete # {0}?', $division->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Divisions'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Division'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List User Profiles'), ['controller' => 'UserProfiles', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New User Profile'), ['controller' => 'UserProfiles', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="divisions view large-9 medium-8 columns content">
    <h3><?= h($division->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($division->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($division->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($division->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($division->modified) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related User Profiles') ?></h4>
        <?php if (!empty($division->user_profiles)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('User Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Designation') ?></th>
                <th scope="col"><?= __('Department Id') ?></th>
                <th scope="col"><?= __('Division Id') ?></th>
                <th scope="col"><?= __('Phone Number') ?></th>
                <th scope="col"><?= __('Gender') ?></th>
                <th scope="col"><?= __('Blood Group') ?></th>
                <th scope="col"><?= __('Address') ?></th>
                <th scope="col"><?= __('User Type') ?></th>
                <th scope="col"><?= __('Semester') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($division->user_profiles as $userProfiles): ?>
            <tr>
                <td><?= h($userProfiles->id) ?></td>
                <td><?= h($userProfiles->user_id) ?></td>
                <td><?= h($userProfiles->name) ?></td>
                <td><?= h($userProfiles->designation) ?></td>
                <td><?= h($userProfiles->department_id) ?></td>
                <td><?= h($userProfiles->division_id) ?></td>
                <td><?= h($userProfiles->phone_number) ?></td>
                <td><?= h($userProfiles->gender) ?></td>
                <td><?= h($userProfiles->blood_group) ?></td>
                <td><?= h($userProfiles->address) ?></td>
                <td><?= h($userProfiles->user_type) ?></td>
                <td><?= h($userProfiles->semester) ?></td>
                <td><?= h($userProfiles->created) ?></td>
                <td><?= h($userProfiles->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'UserProfiles', 'action' => 'view', $userProfiles->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'UserProfiles', 'action' => 'edit', $userProfiles->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'UserProfiles', 'action' => 'delete', $userProfiles->id], ['confirm' => __('Are you sure you want to delete # {0}?', $userProfiles->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
